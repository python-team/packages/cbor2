cbor2 (5.6.5-1) unstable; urgency=medium

  * New upstream version 5.6.5

 -- Georges Khaznadar <georgesk@debian.org>  Tue, 15 Oct 2024 19:59:40 +0200

cbor2 (5.6.4-1) unstable; urgency=medium

  * New upstream version 5.6.4

 -- Georges Khaznadar <georgesk@debian.org>  Fri, 07 Jun 2024 13:18:35 +0200

cbor2 (5.6.3-1) unstable; urgency=medium

  * New upstream version 5.6.3

 -- Georges Khaznadar <georgesk@debian.org>  Wed, 06 Mar 2024 17:08:11 +0100

cbor2 (5.6.2-1) unstable; urgency=medium

  * New upstream version 5.6.2; Closes: #1064416

 -- Georges Khaznadar <georgesk@debian.org>  Mon, 26 Feb 2024 13:56:11 +0100

cbor2 (5.6.1-1) unstable; urgency=medium

  * New upstream version 5.6.1

 -- Georges Khaznadar <georgesk@debian.org>  Tue, 06 Feb 2024 19:11:09 +0100

cbor2 (5.6.0-1) unstable; urgency=medium

  * New upstream version 5.6.0

 -- Georges Khaznadar <georgesk@debian.org>  Mon, 22 Jan 2024 17:53:08 +0100

cbor2 (5.5.1-1) unstable; urgency=medium

  * New upstream version 5.5.1
  * Declared myself as uploader. Closes: #1055355
  * Bumped Standards-Version: 4.6.2
  * Added a build-dependency on python3-sphinx-autodoc-typehints
  * Disabled dh_auto_test since it failed, due to PySide2 not providing
    the module QtTest.

 -- Georges Khaznadar <georgesk@debian.org>  Thu, 23 Nov 2023 13:08:28 +0100

cbor2 (5.4.6-2) unstable; urgency=medium

  [ Helmut Grohne ]
  * Add missing build dependency libpython3-all-dev (Closes: #1033054)

 -- Bastian Germann <bage@debian.org>  Thu, 16 Mar 2023 15:22:42 +0100

cbor2 (5.4.6-1) unstable; urgency=medium

  * New upstream version 5.4.6
  * Remove i386 patch

 -- Bastian Germann <bage@debian.org>  Fri, 09 Dec 2022 12:31:32 +0100

cbor2 (5.4.5-1) unstable; urgency=medium

  * New upstream version 5.4.5
  * Add upstream patch to fix i386 build
  * Drop OverflowError patch

 -- Bastian Germann <bage@debian.org>  Fri, 02 Dec 2022 10:20:14 +0000

cbor2 (5.4.3-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + python-cbor2-doc: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 27 Nov 2022 15:23:31 +0000

cbor2 (5.4.3-2) unstable; urgency=medium

  * Use pyproject for build

 -- Bastian Germann <bage@debian.org>  Sat, 15 Oct 2022 10:43:14 +0200

cbor2 (5.4.3-1) unstable; urgency=medium

  * New upstream version 5.4.3
  * Rebase patches

 -- Bastian Germann <bage@debian.org>  Thu, 22 Sep 2022 12:08:54 +0200

cbor2 (5.4.2-1) unstable; urgency=medium

  * Fix missing-build-dependency-for-dh-addon
  * Make doc reproducible
  * New upstream version 5.4.2

 -- Bastian Germann <bage@debian.org>  Mon, 18 Oct 2021 22:38:54 +0200

cbor2 (5.4.1-1) unstable; urgency=medium

  * New upstream version 5.4.1
  * Let patches apply on the new version

 -- Bastian Germann <bastiangermann@fishpost.de>  Wed, 25 Aug 2021 13:50:07 +0200

cbor2 (5.2.0-4) unstable; urgency=medium

  * Annotate more B-D with :native for cross build
  * Apply lintian suggestions

 -- Bastian Germann <bastiangermann@fishpost.de>  Thu, 18 Feb 2021 11:04:36 +0100

cbor2 (5.2.0-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Bastian Germann ]
  * Annotate B-D setuptools-scm with :native

 -- Bastian Germann <bastiangermann@fishpost.de>  Wed, 17 Feb 2021 16:17:35 +0100

cbor2 (5.2.0-2) unstable; urgency=medium

  * d/control: Set Debian Python Team as Maintainer

 -- Bastian Germann <bastiangermann@fishpost.de>  Fri, 16 Oct 2020 17:46:07 +0200

cbor2 (5.2.0-1) unstable; urgency=medium

  * Initial release (Closes: #972039)

 -- Bastian Germann <bastiangermann@fishpost.de>  Sun, 11 Oct 2020 13:34:18 +0200
